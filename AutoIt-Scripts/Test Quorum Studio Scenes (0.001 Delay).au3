#include <Constants.au3>

; Slow down the speed of sending values.
AutoItSetOption("SendKeyDelay", 35)
AutoItSetOption("SendKeyDownDelay", 35)

; Run the command line
Run("cmd.exe")

; Run the test 10 times.
For $i = 1 To 10 Step 1

   ; Wait for the command prompt to become active.
   Sleep(1500)

   ; Copy the template file over the file we'll use. This ensures a clean slate for each run.
   Send("cd " & @ScriptDir & "/SceneTestApplication/Scenes{ENTER}")
   Sleep(1000)
   Send("xcopy .\Scene-Template.qs .\Scene.qs /Y{ENTER}")
   Sleep(1000)

   ; Navigate to the application folder
   Send("cd ../../QuorumStudio0.001Delay{ENTER}")
   Sleep(1000)

   ; Run the application via Java
   Send("java -jar Run/QuorumStudio.jar{ENTER}")
   ; Wait for the application to become active
   Sleep(25000)

   ; Open the scene file that should already be selected by default.
   Send("{ENTER}")
   Sleep(1500)

   ; Return to the project tree.
   Send("{CTRLdown}")
   Sleep(500)
   Send("{1}")
   Sleep(500)
   Send("{CTRLup}")
   Sleep(1500)

   ; Change to the scene tree. This automatically shows the 3D layer.
   Send("{CTRLdown}")
   Sleep(500)
   Send("{TAB}")
   Sleep(500)
   Send("{CTRLup}")
   Sleep(1500)

   ; Return to the scene. Delay before continuing so we can distinguish events between GUI/scene interaction.
   Send("{CTRLdown}")
   Sleep(500)
   Send("{2}")
   Sleep(500)
   Send("{CTRLup}")
   Sleep(15000)

   ; Start sending commands to manipulate the items in the scene.
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(1000)

   SEND("{ENTER}")
   Sleep(1000)

   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(1000)

   SEND("{ESCAPE}")
   Sleep(1000)

   Send("{UP}")
   Sleep(500)
   Send("{UP}")
   Sleep(1000)

   SEND("{ENTER}")
   Sleep(1000)

   Send("{DOWN}")
   Sleep(500)
   Send("{DOWN}")
   Sleep(1000)

   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(1000)

   SEND("{ESCAPE}")
   Sleep(1000)

   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(1000)

   SEND("{ENTER}")
   Sleep(1000)

   Send("{DOWN}")
   Sleep(500)
   Send("{DOWN}")
   Sleep(1000)

   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(1000)

   SEND("{ESCAPE}")
   Sleep(1000)

   Send("{DOWN}")
   Sleep(500)
   Send("{DOWN}")
   Sleep(1000)

   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(500)
   SEND("{RIGHT}")
   Sleep(1000)

   Send("{DOWN}")
   Sleep(1000)

   SEND("{ENTER}")
   Sleep(1000)

   Send("{UP}")
   Sleep(1000)

   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(1000)

   Send("{CTRLdown}")
   Sleep(500)
   Send("{UP}")
   Sleep(500)
   Send("{CTRLup}")
   Sleep(1000)

   SEND("{ESCAPE}")
   Sleep(1000)

   Send("{UP}")
   Sleep(500)
   Send("{UP}")
   Sleep(1000)

   SEND("{ENTER}")
   Sleep(1000)

   Send("{CTRLdown}")
   Sleep(500)
   Send("{UP}")
   Sleep(500)
   Send("{CTRLup}")
   Sleep(1000)

   Send("{DOWN}")
   Sleep(1000)

   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(1000)

   SEND("{ESCAPE}")
   Sleep(1000)

   Send("{UP}")
   Sleep(1000)

   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(500)
   SEND("{LEFT}")
   Sleep(1000)

   SEND("{ENTER}")
   Sleep(1000)

   SEND("{RIGHT}")
   Sleep(1000)

   Send("{CTRLdown}")
   Sleep(500)
   Send("{UP}")
   Sleep(500)
   Send("{CTRLup}")
   Sleep(1000)

   Send("{DOWN}")
   Sleep(500)
   Send("{DOWN}")
   Sleep(1000)

   SEND("{ESCAPE}")
   Sleep(1000)

   ; Put a sizable delay to distinguish events in the scene from interactions with the GUI.
   Sleep(15000)

   ; Close the tab.
   Send("{CTRLdown}")
   Sleep(500)
   Send("{F4}")
   Sleep(500)
   Send("{CTRLup}")
   Sleep(2000)

   ; Close the application.
   Send("{ALTdown}")
   Sleep(500)
   Send("{F4}")
   Sleep(500)
   Send("{ALTup}")
   Sleep(2000)

Next

Send("REM Finished running tests.{ENTER}")

Exit