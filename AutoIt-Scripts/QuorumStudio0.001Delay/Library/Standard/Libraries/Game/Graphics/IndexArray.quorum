package Libraries.Game.Graphics

use Libraries.Game.Graphics.IndexData
use Libraries.Containers.Array
use Libraries.Containers.Integer32BitArray

class IndexArray is IndexData

    /*
    This action will initialize the IndexArray to allow for the given number of
    indices.
    */
    system action Load(integer maximumSize)

    /*
    This action will return the current number of indices in this IndexArray.
    */
    system action GetSize returns integer

    /*
    This action will return the maximum number of indices this IndexArray can contain.
    */
    system action GetMaxSize returns integer

    /*
    This action will set the indices of the IndexArray using the given array,
    starting at the offset index and iterating through the given number of 
    indices.
    */
    action SetIndices(Integer32BitArray indices, integer offset, integer count)
        SendToBridgeArray(indices)
        SetIndices(offset, count)
    end

    private system action SetIndices(integer offset, integer count)

    /*
    Binds this set of indices for use by certain OpenGL functions. For an
    IndexArray, this does nothing.
    */
    system action Bind

    /*
    Unbinds this set of indices so it can't be used by certain OpenGL functions. 
    For an IndexArray, this does nothing.
    */
    system action Unbind

    /*
    Used to force OpenGL to create a new buffer handle. Used for context loss.
    */
    system action Reload

    /*
    Clears the underlying buffer used to store the values of this IndexArray.
    */
    system action Clear

    /*
    This action is used to release the memory used by this object.
    */
    system action Dispose

    action Put(Integer32BitArray values)
        SendToBridgeArray(values)
        PutBridgeArray()
    end

    /* Sends an entire Quorum array to be stored in the bridge array within the plugins. */
    private system action SendToBridgeArray(Integer32BitArray array)

    private system action PutBridgeArray

    system action SetPosition(integer position)

end