package Libraries.Interface.Controls.Charts

use Libraries.Interface.Controls.Control
use Libraries.Interface.AccessibilityManager
use Libraries.Interface.Behaviors.Behavior
use Libraries.Interface.Events.BehaviorEvent
use Libraries.Game.GameStateManager
use Libraries.Game.Graphics.Color
use Libraries.Game.Graphics.ColorGroup
use Libraries.Game.Graphics.Gradient
use Libraries.Game.Graphics.Drawable
use Libraries.Game.Graphics.Texture
use Libraries.Game.Graphics.TextureRegion
use Libraries.Game.Graphics.Font
use Libraries.Game.Graphics.Label
use Libraries.System.File
use Libraries.Interface.Views.ImageControlView
use Libraries.Interface.Views.LabelBoxView
use Libraries.Interface.Views.ControlView
use Libraries.Interface.Views.View2D
use Libraries.Interface.Layouts.LayoutProperties
use Libraries.Interface.Layouts.FlowLayout
use Libraries.Interface.Controls.Icon
use Libraries.Containers.Array
use Libraries.Interface.Events.FocusEvent
use Libraries.Interface.Controls.List
use Libraries.Game.InputTable
use Libraries.Game.InputSet
use Libraries.Interface.Events.KeyboardEvent
use Libraries.Interface.Controls.ControlLabel
use Libraries.Interface.Layouts.ManualLayout
use Libraries.Interface.Layouts.BarChartLayout
use Libraries.Interface.Selections.ChartSelection
use Libraries.Controls.Charts.ChartOptions
use Libraries.Compute.Math

/*
    The BarChart class is Chart object that inherits from Control and like other
    UI elements it is added to the Game class. The Bar Chart is used to represent
    categorical data with rectangular bars that have a height proportional to the 
    data they represent. By default, the chart has no added bars and has a scale 
    that goes from 0 to 1. The title label, axis labels, and scale can be modified, 
    and any number of bars can be added. 

    Attribute: Author Gabriel Contreras

    Attribute: Example

    use Libraries.Interface.Controls.Charts
    use Libraries.Game.Game

    class Main is Game
        action Main
            StartGame()
        end

        action CreateGame
            BarChart chart
            Add(chart)
        end
    end
*/
class BarChart is SharedBarChartParent
    LayoutProperties labelProperties

    on create
        BarChartLayout lay
        SetLayout(lay)

        LayoutProperties properties = GetDefaultLayoutProperties()
        properties:SetHorizontalLayoutMode(properties:MAINTAIN_ASPECT_RATIO)
        properties:SetVerticalLayoutMode(properties:STANDARD)
        Font font
        font:LoadFont("Arial")
        properties:SetFont(font)
        properties:SetFontSize(16)

        Color color
        Gradient gradient
        Color gray = color:LightGray()
        Color lightGray = color:CustomColor(0.9, 0.9, 0.9, 1)
        gradient:Set(gray, gray, lightGray, lightGray)

        properties:SetBackgroundColor(gradient)
        properties:SetBorderColor(color:Black())
        properties:SetBorderThickness(2)
        SetName("Bar Chart")

        SetInputGroup("Chart")
        SetFocusable(true)
        SetAccessibilityCode(parent:Item:ITEM)
    end

    /*
    This is called by GenerateInfoTree to generate the summary that is heard when
    you first focus on the chart. Also the highest level of the information tree
    of  the chart.
    */
    private action GenerateSummary returns text
        SetDescription("Bar Chart, " + GetNumberOfBars() + " bars, " + GetMinimumTick() + " to " + GetScaleMax() + 
            ". Use the arrow keys to navigate the chart.")
        return GetDescription()
    end
    
    

end

