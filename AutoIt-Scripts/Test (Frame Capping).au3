#include <Constants.au3>

; Run the command line
Run("cmd.exe")

; Wait for the command prompt to become active.
Sleep(1500)

; Now that the prompt is active, navigate to the frame-capping enabled test and run it via Java
Send("cd " & @ScriptDir & "/CappedFrameTest/Run{ENTER}")
Sleep(500)
Send("java -jar TestApplication.jar{ENTER}")
Sleep(10000)

Send("{TAB 3}")
Sleep(500)
Send("{DOWN}")
Sleep(500)
Send("{RIGHT}")
Sleep(500)
Send("{DOWN 4}")
Sleep(500)
Send("{RIGHT}")
Sleep(500)
Send("{DOWN 2}")
Sleep(500)
Send("{TAB}")
Sleep(1000)

Send("Hello world!")
Sleep(1000)
Send("{ENTER}")
Sleep(1000)
Send("This is additional text.")
Sleep(1000)
Send("{ENTER}")
Sleep(1000)
Send("The screen reader may automatically fire events as text is entered.")
Sleep(1000)
Send("{ENTER}")
Sleep(1000)
Send("Additional events occur when text is navigated.")
Sleep(1000)
Send("{ENTER}")
Sleep(1000)


Send("{UP 2}")
Sleep(1000)
Send("{RIGHT 5}")
Sleep(1500)

Send("{TAB}")
Sleep(1000)

Send("{ALT down}")
Sleep(500)
Send("{F4}")
Sleep(500)
Send("{ALT up}")
Sleep(1000)

Exit