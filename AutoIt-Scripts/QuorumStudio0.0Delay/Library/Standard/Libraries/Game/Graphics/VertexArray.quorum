package Libraries.Game.Graphics

use Libraries.Containers.Number32BitArray
use Libraries.Game.Graphics.VertexData
use Libraries.Containers.Array

class VertexArray is VertexData
  
    /*
    This action will initialize the VertexArray to use the given number of
    vertices and to use the provided VertexAttributes.
    */
    system action Load(integer numVertices, VertexAttributes attributes)
    
    /*
    This action releases the memory stored by this VertexData. This should only
    be used when the VertexData is not in use and will not be needed again!
    */
    system action Dispose

    /*
    This action returns the number of vertices currently stored in this
    VertexData object.
    */
    system action GetSize returns integer

    /*
    This action returns the maximum number of vertices that can be stored in
    this VertexData object.
    */
    system action GetMaxSize returns integer

    /*
    This action will return the VertexAttributes that describe this VertexData.
    */
    system action GetAttributes returns VertexAttributes

    action UpdateVertices(integer targetOffset, Number32BitArray vertices, integer sourceOffset, integer count)
        SendToBridgeArray(vertices, sourceOffset, count)
        UpdateVerticesNative(targetOffset, sourceOffset, count)
    end

    private system action UpdateVerticesNative(integer targetOffset, integer sourceOffset, integer count)

    /*
    Used to invalidate the data when applicable, e.g. due to context loss.
    */
    system action Reload

    /*
    This action directly copies an array of vertices into the buffer of this
    VertexArray.
    */
    action FillVertexBuffer(Number32BitArray vertices)
        SendToBridgeArray(vertices)
        PopulateVertexBuffer()
    end

    action SetVertices(Number32BitArray vertices)
        SendToBridgeArray(vertices)
        PopulateVertexBuffer()
    end

    private system action SendToBridgeArray(Number32BitArray vertices)
    
    /*
    Directly copies the data stored in the bridge array into the buffer
    stored in the plugin class.
    */
    private system action PopulateVertexBuffer

    /* Used to inform the plugin that we are about to send a new array of
    doubles with the given length. */
    private system action PrepareBridgeArray(integer length)

    /* Sends part of a Quorum array to be stored in the bridge array within the plugins. */
    private action SendToBridgeArray(Number32BitArray array, integer offset, integer count)
        PrepareBridgeArray(count)
        integer index = offset
        repeat count times
            SendToBridgeArray(index, array:Get(index))
            index = index + 1
        end
    end

    /* Sends a value to be stored in the bridge array at the given index. */
    private system action SendToBridgeArray(integer index, number value)
end