{
    "Layers": {
        "0": {
            "Name": "Default Layer3D", 
            "Class": "Layer3D", 
            "Camera": {
                "Class": "PerspectiveCamera", 
                "Field of View": 67.0, 
                "Position": [0.0, 3.0, -5.0], 
                "Direction": [0.0, -0.5144957304000854, 0.8574929237365723], 
                "Up": [0.0, 0.8574929237365723, 0.5144957304000854], 
                "Size": [4521.0, 1811.0], 
                "Far": 1000.0, 
                "Near": 1.0
            }, 
            "Physics": {
                "Enabled": false
            }, 
            "Items": {
                "2": {
                    "Class": "Model", 
                    "Name": "Cube 2", 
                    "Description": "", 
                    "Position": [-6.0, 0.0, 2.0], 
                    "Size": [1.0, 1.0, 1.0], 
                    "Scale": [1.0, 1.0, 1.0], 
                    "Transform": [1.0, 0.0, 0.0, -6.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 0.0, 0.0, 0.0, 1.0], 
                    "Type": 1, 
                    "Color": [1.0, 1.0, 1.0, 1.0], 
                    "Physics Enabled": false, 
                    "Euler Angles": [0.0, 0.0, 0.0], 
                    "Scale": [1.0, 1.0, 1.0]
                }, 
                "3": {
                    "Class": "Model", 
                    "Name": "Cube 3", 
                    "Description": "", 
                    "Position": [-4.0, 0.0, 2.0], 
                    "Size": [1.0, 1.0, 1.0], 
                    "Scale": [1.0, 1.0, 1.0], 
                    "Transform": [1.0, 0.0, 0.0, -4.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 0.0, 0.0, 0.0, 1.0], 
                    "Type": 1, 
                    "Color": [1.0, 1.0, 1.0, 1.0], 
                    "Physics Enabled": false, 
                    "Responsiveness": 0, 
                    "Euler Angles": [0.0, 0.0, 0.0], 
                    "Scale": [1.0, 1.0, 1.0]
                }, 
                "4": {
                    "Class": "Model", 
                    "Name": "Sphere", 
                    "Description": "", 
                    "Position": [-1.0, 0.0, 2.0], 
                    "Size": [1.0, 1.0, 1.0], 
                    "Scale": [1.0, 1.0, 1.0], 
                    "Transform": [1.0, 0.0, 0.0, -1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 2.0, 0.0, 0.0, 0.0, 1.0], 
                    "Type": 3, 
                    "Color": [0.6000000238418579, 1.0, 1.0, 1.0], 
                    "Physics Enabled": false, 
                    "Euler Angles": [0.0, 0.0, 0.0], 
                    "Scale": [1.0, 1.0, 1.0]
                }, 
                "5": {
                    "Class": "Model", 
                    "Name": "Cube 4", 
                    "Description": "", 
                    "Position": [-6.0, 0.0, 0.0], 
                    "Size": [1.0, 1.0, 1.0], 
                    "Scale": [1.0, 1.0, 1.0], 
                    "Transform": [1.0, 0.0, 0.0, -6.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0], 
                    "Type": 1, 
                    "Color": [0.800000011920929, 0.800000011920929, 0.800000011920929, 1.0], 
                    "Physics Enabled": false, 
                    "Euler Angles": [0.0, 0.0, 0.0], 
                    "Scale": [1.0, 1.0, 1.0]
                }, 
                "6": {
                    "Class": "Model", 
                    "Name": "Cylinder", 
                    "Description": "", 
                    "Position": [4.0, 0.0, 1.0], 
                    "Size": [1.0, 1.0, 1.0], 
                    "Scale": [1.0, 1.0, 1.0], 
                    "Transform": [1.0, 0.0, 0.0, 4.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0], 
                    "Type": 2, 
                    "Color": [0.800000011920929, 0.800000011920929, 1.0, 1.0], 
                    "Physics Enabled": false, 
                    "Euler Angles": [0.0, 0.0, 0.0], 
                    "Scale": [1.0, 1.0, 1.0]
                }, 
                "7": {
                    "Class": "Model", 
                    "Name": "Cylinder 1", 
                    "Description": "", 
                    "Position": [4.0, 0.0, -1.0], 
                    "Size": [1.0, 1.0, 1.0], 
                    "Scale": [1.0, 1.0, 1.0], 
                    "Transform": [1.0, 0.0, 0.0, 4.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 1.0, -1.0, 0.0, 0.0, 0.0, 1.0], 
                    "Type": 2, 
                    "Color": [0.800000011920929, 0.800000011920929, 1.0, 1.0], 
                    "Physics Enabled": false, 
                    "Euler Angles": [0.0, 0.0, 0.0], 
                    "Scale": [1.0, 1.0, 1.0]
                }
            }, 
            "Lights": {
                "0": {
                    "Class": "AmbientLight", 
                    "Color": [0.20000000298023224, 0.20000000298023224, 0.20000000298023224, 1.0]
                }, 
                "1": {
                    "Class": "DirectionalLight", 
                    "Color": [0.800000011920929, 0.800000011920929, 0.800000011920929, 1.0], 
                    "Direction": [-1.0, -4.0, 2.0]
                }
            }
        }, 
        "1": {
            "Name": "Default Layer2D", 
            "Class": "Layer2D", 
            "Camera": {
                "Class": "OrthographicCamera", 
                "Zoom": 1.0, 
                "EDITOR_Zoom": 1.0, 
                "Position": [400.0, 300.0, 10000.0], 
                "EDITOR_Position": [400.0, 300.0], 
                "Direction": [0.0, 0.0, -1.0], 
                "Up": [0.0, 1.0, 0.0], 
                "Size": [800.0, 600.0], 
                "Far": 20000.0, 
                "Near": 0.0
            }, 
            "Physics": {
                "Enabled": false
            }, 
            "Items": {

            }
        }
    }
}