The AutoIt scripts in this folder can be used to test use of the GUI in a simple application or in Quorum Studio.
Events passed through the message pump are recorded and stored in the "Log" folders inside the individual run environment folders.
Once a script begins, it will begin mimicking input to the system. Do not interact with the system while the scripts are running.

Quorum Studio maintains a history of open projects and files which persist between runs. In order for each Quorum Studio test to run correctly, the environment is expected to be in a certain state when the test is run.
Before running the tests, follow the steps below to prime the environment.

Before running the Quorum Studio Interface tests:
* Open Quorum Studio.
* Close all open projects and files.
* Close Quorum Studio.

Before running the Quorum Studio Scene tests:
* Open Quorum Studio.
* Open the SceneTestApplication project.
* Close any other open projects.
* In the project tree, under the SceneTestApplication, open the Scenes folder, and make sure all other folders are collapsed.
* Select the file Scene.qs by left-clicking it once or navigating to it via the keyboard.
* Close Quorum Studio.

The steps above will ensure Quorum Studio opens in the expected state for the tests. No such preparation is needed for the frame capping test.