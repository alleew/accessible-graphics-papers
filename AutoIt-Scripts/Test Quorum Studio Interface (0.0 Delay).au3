#include <Constants.au3>

; Slow down the speed of sending values.
AutoItSetOption("SendKeyDelay", 35)
AutoItSetOption("SendKeyDownDelay", 35)

; Run the command line
Run("cmd.exe")

; Wait for the command prompt to become active.
Sleep(2000)

Send("cd " & @ScriptDir & "\QuorumStudio0.0Delay{ENTER}")
Sleep(1000)

; Run the test 10 times.
For $i = 1 To 10 Step 1

   ; Run the application via Java
   Send("java -jar Run/QuorumStudio.jar{ENTER}")
   ; Wait for the application to become active
   Sleep(15000)

   ; Open the large Quorum project. We'll spend part of the test navigating its project tree.
   Send("^{o}")
   Sleep(1000)
   Send(@ScriptDir & "\QuorumLargeProject\Project\Project.qp{ENTER}")
   Sleep(15000)

   Send("{RIGHT 2}")
   Sleep(200)
   Send("{DOWN 3}")
   Sleep(200)
   Send("{RIGHT 5}")
   Sleep(200)
   Send("{DOWN 2}")
   Sleep(200)
   Send("{RIGHT}")
   Sleep(200)
   Send("{DOWN 5}")
   Sleep(200)
   Send("{RIGHT}")
   Sleep(200)

   Send("{DOWN 2}")
   Sleep(200)
   Send("{RIGHT}")
   Sleep(200)
   Send("{DOWN 42}")
   Sleep(200)
   Send("{LEFT}")
   Sleep(200)

   Send("{RIGHT 2}")
   Sleep(200)
   Send("{DOWN 6}")
   Sleep(200)
   Send("{LEFT 14}")
   Sleep(200)
   Send("{DOWN}")
   Sleep(200)

   Send("{RIGHT}")
   Sleep(200)
   Send("{DOWN 2}")
   Sleep(200)
   Send("{RIGHT}")
   Sleep(200)
   Send("{DOWN 13}")
   Sleep(200)

   Send("{RIGHT}")
   Sleep(200)
   Send("{DOWN 3}")
   Sleep(200)
   Send("{UP 12}")
   Sleep(5000)
   Send("{HOME}")
   Sleep(2000)
   Send("{HOME}")
   Sleep(2000)

   Send("+{F10}")
   Sleep(2000)
   Send("{DOWN 6}")
   Sleep(2000)
   Send("{ENTER}")
   Sleep(3000)

   ; Open the second Quorum project. This one contains a large text file we will navigate.
   Send("^{o}")
   Sleep(1000)
   Send(@ScriptDir & "\GameClassFile\Project\Project.qp{ENTER}")
   Sleep(10000)

   Send("{RIGHT 5}")
   Sleep(200)
   Send("{ENTER}")
   Sleep(200)
   Send("{DOWN 30}")
   Sleep(200)
   Send("{RIGHT 20}")
   Sleep(200)

   Send("{SHIFTdown}")
   Sleep(200)
   Send("{DOWN 6}")
   Sleep(200)
   Send("{SHIFTup}")
   Sleep(200)

   Send("{END}")
   Sleep(200)
   Send("^{a}")

   Send("{UP 3}")
   Sleep(200)
   Send("{END}")
   Sleep(200)
   Send("^{HOME}")
   Sleep(200)
   Send("{DOWN 4}")
   Sleep(200)

   Send("Hello world")
   Sleep(200)
   Send("+{HOME}")
   Sleep(200)
   Send("{BACKSPACE}")
   Sleep(2000)

   Send("^{F4}")
   Sleep(2000)
   Send("^{1}")
   Sleep(5000)
   Send("{HOME}")
   Sleep(2000)
   Send("{HOME}")
   Sleep(2000)
   Send("+{F10}")
   Sleep(2000)
   Send("{DOWN 6}")
   Sleep(2000)
   Send("{ENTER}")
   Sleep(3000)
   Send("!{F4}")
   Sleep(4000)

Next

Send("REM Finished running tests.{ENTER}")

Exit