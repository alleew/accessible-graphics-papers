package Libraries.Compute.Statistics.Columns

use Libraries.Containers.Array
use Libraries.Containers.HashTable
use Libraries.Containers.Iterator
use Libraries.Compute.Statistics.DataFrameColumnCalculation
use Libraries.Compute.Statistics.DataFrameColumn
use Libraries.Compute.Statistics.Columns.NumberColumn
use Libraries.Compute.Statistics.Columns.IntegerColumn
use Libraries.Compute.Statistics.Columns.BooleanColumn

/*
    TextColumn is a DataFrameColumn that contains Number objects. These objects can be undefined
    or not. 

    Attribute: Author Andreas Stefik
*/
class TextColumn is DataFrameColumn
    /* This is the new system, which is rows. */
    Array<Text> rows

    action Add(text value)
        if value = undefined
            rows:Add(undefined)
            parent:DataFrameColumn:undefinedSize = GetUndefinedSize() + 1
            return now
        end

        rows:Add(value)
    end

    action SendValueTo(integer index, DataFrameColumnCalculation calculation)
        Text num = rows:Get(index)
        calculation:Add(num)
    end

    action SetSize(integer size)
        rows:SetSize(size)
    end

    action GetSize returns integer
        return rows:GetSize()
    end

    action IsUndefined(integer row) returns boolean
        return rows:Get(row) = undefined
    end

    action Get(integer row) returns Text
        return rows:Get(row)
    end

    action GetAsText(integer index) returns text
        Text value = rows:Get(index)
        if value = undefined
            return undefined
        else
            return value:GetValue()
        end
    end

    action SetAsText(integer index, text value)
        rows:Set(index, value)
    end

    action IsTextColumn returns boolean
        return true
    end

    action Swap(integer left, integer right)
        Text temp = undefined
        temp = rows:Get(left)
        rows:Set(left, rows:Get(right))
        rows:Set(right, temp)
    end

    action Move(integer left, integer right)
        rows:Set(right, rows:Get(left))
    end

    action Copy(integer rowStart, integer rowEnd) returns DataFrameColumn
        TextColumn column
        column:SetHeader(GetHeader())

        i = rowStart
        repeat while i < rowEnd
            Text value = rows:Get(i)
            if value = undefined
                column:rows:Add(undefined)
            else
                Text value2
                value2:SetValue(value:GetValue())
                column:rows:Add(value2)
            end
            i = i + 1
        end

        return column
    end

    action Copy returns DataFrameColumn
        return Copy(0, rows:GetSize())
    end

    action ToText returns text
        text result = ""
        text lf = result:GetLineFeed()
        i = 0
        repeat while i < GetSize()
            Text t = rows:Get(i)
            if t not= undefined
                text value = t:GetValue()
                result = result + value + lf
            else
                result = result + GetUndefinedText() + lf
            end
            
            i = i + 1
        end
        return result
    end
end